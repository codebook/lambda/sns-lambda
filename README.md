# SNS + Lambda


_tbd_


## References

* [SNS.publish](http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/SNS.html#publish-property)
* [Subscribe to a Topic](http://docs.aws.amazon.com/sns/latest/dg/SubscribeTopic.html)
* [Tutorial: Using AWS Lambda with Amazon SNS](http://docs.aws.amazon.com/lambda/latest/dg/with-sns-example.html)
* [Invoking Lambda functions using Amazon SNS notifications](http://docs.aws.amazon.com/sns/latest/dg/sns-lambda.html)
* [Invoking AWS Lambda functions via Amazon SNS](https://aws.amazon.com/blogs/mobile/invoking-aws-lambda-functions-via-amazon-sns/)

